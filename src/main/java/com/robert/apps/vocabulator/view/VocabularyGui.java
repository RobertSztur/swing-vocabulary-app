package com.robert.apps.vocabulator.view;

import com.robert.apps.vocabulator.exceptions.Exceptions;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import static com.robert.apps.vocabulator.controler.GameConstans.*;

public class VocabularyGui {

    private JFrame window;
    private JMenuBar menuBar;
    private JMenu files;
    private JMenuItem open;
    private JLabel score;
    private JLabel scorePoints;
    private JLabel increment;
    private JLabel decrement;
    private JLabel appName;
    private JLabel word;
    private JLabel autor;
    private JTextField textField;
    private JLabel img;
    private JFileChooser fileChooser;
    private JButton start;
    private JButton check;

    public void run(ActionListener actionListener) throws IOException {
        createAndShowGui();
        setController(actionListener);
    }

    private void createAndShowGui() throws IOException {
        try {
            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        window = new JFrame("Vocabulary Game");
        window.setLayout(null);
        window.setMinimumSize(new Dimension(500, 500));
        window.setResizable(false);
        window.getContentPane().setBackground(new Color(122, 176, 191));

        menuBar = new JMenuBar();
        menuBar.setPreferredSize(new Dimension(500, 20));
        menuBar.setBackground(new Color(184, 187, 182));
        menuBar.setBorderPainted(false);
        window.setJMenuBar(menuBar);

        files = new JMenu("Files");
        files.setForeground(new Color(0, 0, 0));
        menuBar.add(files);

        open = new JMenuItem("Open file");
        open.setActionCommand(OPEN);
        files.add(open);

        fileChooser = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
        fileChooser.setFileFilter(new FileNameExtensionFilter("*.csv", "csv"));


        score = new JLabel("Score");
        score.setForeground(Color.BLACK);
        score.setFont(new Font("Arial", Font.PLAIN, 25));
        score.setBounds(280, 0, 250, 20);
        window.getContentPane().add(score);


        scorePoints = new JLabel("100");
        scorePoints.setForeground(Color.BLACK);
        scorePoints.setFont(new Font("Arial", Font.PLAIN, 25));
        scorePoints.setBounds(400, 0, 250, 20);
        window.getContentPane().add(scorePoints);

        increment = new JLabel("+1");
        increment.setForeground(Color.red);
        increment.setFont(new Font("Serif", Font.PLAIN, 15));
        increment.setBounds(480, 15, 20, 10);
        window.getContentPane().add(increment);

        decrement = new JLabel("-1");
        decrement.setForeground(Color.red);
        decrement.setFont(new Font("Serif", Font.PLAIN, 15));
        decrement.setBounds(480, 15, 20, 40);
        window.getContentPane().add(decrement);


        appName = new JLabel("VOCABULARY GAME");
        appName.setForeground(Color.BLACK);
        appName.setFont(new Font("Arial", Font.PLAIN, 25));
        appName.setBounds(130, 50, 350, 25);
        window.getContentPane().add(appName);

        word = new JLabel("English word:");
        word.setForeground(Color.RED);
        word.setFont(new Font("Arial", Font.PLAIN, 25));
        word.setBounds(100, 100, 300, 35);
        window.getContentPane().add(word);

        autor = new JLabel("by Robert Sztur");
        autor.setForeground(Color.BLACK);
        autor.setFont(new Font("Arial", Font.ITALIC, 10));
        autor.setBounds(420, 430, 200, 10);
        window.getContentPane().add(autor);

        textField = new JTextField();
        textField.setBounds(100, 150, 300, 30);
        window.getContentPane().add(textField);

        check = new JButton("CHECK");
        check.setBounds(175, 200, 150, 35);
        check.setFont(new Font("Arial", Font.PLAIN, 25));
        check.setBackground(new Color(62, 46, 173));
        check.setForeground(new Color(122, 176, 191));
        check.setBorderPainted(false);
        check.setActionCommand(CHECK);
        window.getContentPane().add(check);

        start = new JButton("START");
        start.setBounds(20, 20, 80, 25);
        start.setBackground(new Color(62, 46, 173));
        start.setForeground(new Color(122, 176, 191));
        start.setBorderPainted(false);
        start.setFont(new Font("Arial", Font.PLAIN, 13));
        start.setActionCommand(START);


        InputStream imageInputStream = getClass().getClassLoader().getResourceAsStream("eng.png");
        byte[] imageData = imageInputStream.readAllBytes();
        img = new JLabel(new ImageIcon(imageData));
        img.setBounds(10, 245, 480, 215);
        window.getContentPane().add(img);


        window.pack();
        window.setVisible(true);
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    private void setController(ActionListener actionListener) {
        open.addActionListener(actionListener);
        check.addActionListener(actionListener);
        start.addActionListener(actionListener);
    }

    public File getFileFromUser() throws Exceptions {
        fileChooser.showOpenDialog(window);
        final File selectedFile = fileChooser.getSelectedFile();
        if (selectedFile != null) {
            return selectedFile;
        }
        throw new Exceptions("No selected file.");
    }

    public void showStart() {
        window.getContentPane().add(start);
        window.repaint();
    }


    public void setScoreLabel(Integer currentPoints) {
        scorePoints.setText(currentPoints.toString());
    }

    public void clearScoreInfo() {
        increment.setText("");
        decrement.setText("");
    }

    public void showWordtoGuess(String wordToGuess) {
        word.setText(wordToGuess);
    }

    public String getUserAnswer() {
        return textField.getText();
    }

    public void updateScoreInfo(boolean isCorrect) {
        if (isCorrect){
            increment.setText("+1");
            decrement.setText("");
        }else {
            decrement.setText(" -1");
            increment.setText("");
        }
    }
    public void clearTextArea() {
        textField.setText("");
    }

    public void showGameOverInfo(Integer currentPoints) {
        JOptionPane.showMessageDialog(window,"You finish word game! Your score is "+scorePoints, "Game over",
        JOptionPane.INFORMATION_MESSAGE);
    }
    private void clearWindow(){

    }
}