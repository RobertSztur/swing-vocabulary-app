package com.robert.apps.vocabulator.controler;

import com.robert.apps.vocabulator.exceptions.CantLoadDictionaryException;
import com.robert.apps.vocabulator.exceptions.Exceptions;
import com.robert.apps.vocabulator.model.DAO.DAO;
import com.robert.apps.vocabulator.model.DAO.csvDAO;
import com.robert.apps.vocabulator.model.Dictionary;
import com.robert.apps.vocabulator.view.VocabularyGui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller implements ActionListener {
    private VocabularyGui gui;
    private Dictionary dictionary;
    private DAO dao;
    private Integer currentPoints;
    private String wordToGuess;

    public Controller(VocabularyGui gui) {
        this.gui = gui;
        this.dao = new csvDAO();
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case GameConstans.OPEN:
                createDictionaryFromFile();
                gui.showStart();
                break;
            case GameConstans.START:
                handleStart();
                break;
            case GameConstans.CHECK:
                handleCheck();
                break;
        }
    }

    private void handleCheck() {
        String userAnswer = gui.getUserAnswer();
        boolean isCorrect = dictionary.isAnswerCorrect(userAnswer);
        gui.setScoreLabel(isCorrect ? ++currentPoints: --currentPoints);
        gui.updateScoreInfo(isCorrect);
        gui.clearTextArea();
        if (dictionary.isAnyLeft()){
            gui.showWordtoGuess(dictionary.getRandomWord());
        }else {
            gui.showGameOverInfo(currentPoints);
        }

    }

    private void createDictionaryFromFile(){
            try {
                dictionary = dao.loadDictionary(gui.getFileFromUser());
                gui.showStart();
            } catch (CantLoadDictionaryException | Exceptions e) {
                System.err.println(e.getMessage());
            }
        }
    public void handleStart() {
        dictionary.fllItemsToDraw();
        currentPoints = 0;
        gui.setScoreLabel(currentPoints);
        gui.clearScoreInfo();
        wordToGuess = dictionary.getRandomWord();
        gui.showWordtoGuess(wordToGuess);
    }
}




