package com.robert.apps.vocabulator;


import com.robert.apps.vocabulator.controler.Controller;
import com.robert.apps.vocabulator.view.VocabularyGui;

import java.io.IOException;

public class Launcher {
    public static void main(String[] args) {
        VocabularyGui gui = new VocabularyGui();
        Controller controller = new Controller(gui);
        javax.swing.SwingUtilities.invokeLater(()-> {
            try {
                gui.run(controller);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

}
