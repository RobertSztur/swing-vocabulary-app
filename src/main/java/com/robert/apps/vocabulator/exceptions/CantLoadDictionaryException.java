package com.robert.apps.vocabulator.exceptions;

public class CantLoadDictionaryException extends  Exception{
    public CantLoadDictionaryException(String message) {
        super(message);
    }
}
