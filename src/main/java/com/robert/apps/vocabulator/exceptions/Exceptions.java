package com.robert.apps.vocabulator.exceptions;

public class Exceptions extends Exception{

    public Exceptions(String message) {
        super(message);
    }
}

