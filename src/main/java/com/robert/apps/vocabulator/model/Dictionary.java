package com.robert.apps.vocabulator.model;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;


public class Dictionary {

    private Collection<Item> items;
    private ArrayList<Item> itemstoDraw;
    private Item currentRandomItem;


    public Dictionary(Collection<Item> items) {
        this.items = items;
        this.itemstoDraw = new ArrayList<>();
        this.itemstoDraw.addAll(items);
    }
    public String getRandomWord() {
        Random generator = new Random();
        int randomIndex = generator.nextInt(itemstoDraw.size());
        currentRandomItem = itemstoDraw.get(randomIndex);
        return currentRandomItem.getEntry();
    }

    @Override
    public String toString() {
        return "Dictionary" + items;
    }

    public boolean isAnswerCorrect(String userAnswer) {
        return currentRandomItem.isContainsTranslations(userAnswer.toLowerCase());
        }

    public void fllItemsToDraw() {

    }

    public boolean isAnyLeft() {
        return !itemstoDraw.isEmpty();
    }
}

