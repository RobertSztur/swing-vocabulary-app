package com.robert.apps.vocabulator.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;

public class Item {

    private String entry;
    private Collection<String> translations;


    public Item(String entryWithTranslations) {
        String[] splited = entryWithTranslations.split(",");
        this.entry = splited [0];
        translations = new HashSet<>();                             //implementowanie kolekcji
        for (int index = 1; index < splited.length; index++) {
            translations.add(splited[index]);
        }
    }

    public String getEntry() {
        return entry;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Item))
            return false;
        Item item = (Item) o;
        return this.entry.equalsIgnoreCase(item.entry);
    }
    @Override
    public int hashCode() {
        return Objects.hash(this.entry.toUpperCase());
    }

    @Override
    public String toString() {
        return this.entry +"->" + this.translations +"\n";
    }

    public boolean isContainsTranslations(String userAnswer) {
        return this.translations.contains(userAnswer);
    }
}
