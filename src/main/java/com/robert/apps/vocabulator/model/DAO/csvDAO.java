package com.robert.apps.vocabulator.model.DAO;

import com.robert.apps.vocabulator.exceptions.CantLoadDictionaryException;
import com.robert.apps.vocabulator.model.Dictionary;
import com.robert.apps.vocabulator.model.Item;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Scanner;

public class csvDAO implements DAO {

    @Override
    public Dictionary loadDictionary(File file) throws CantLoadDictionaryException {
        Collection<Item> items = new HashSet<>();
        try (Scanner input = new Scanner(file)){
            while (input.hasNextLine()){
                items.add(new Item(input.nextLine().toLowerCase()));
            }
            return new Dictionary(items);

        } catch (IOException e) {
            throw new CantLoadDictionaryException("Cant load dictionary from file");
        }
    }
}
