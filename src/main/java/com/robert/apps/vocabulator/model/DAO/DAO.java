package com.robert.apps.vocabulator.model.DAO;

import com.robert.apps.vocabulator.exceptions.CantLoadDictionaryException;
import com.robert.apps.vocabulator.model.Dictionary;

import java.io.File;

public interface DAO {
    Dictionary loadDictionary(File file) throws CantLoadDictionaryException;
}
